/**
 * @description pkg-updater的配置, 详细参数见pkg-updater(https://www.npmjs.com/package/pkg-updater)
 * @type {{
 *   registry: String?,
 *   tag: String?,
 *   level: String?,
 *   checkInterval: Number?
 *   onVersionChange: Function?
 *   logFile: String
 * }}
 */
export const updateConfig = {}

/**
 * @description 模板列表, url仅接受tar.gz格式压缩包下载地址, 填入tplName优先从项目目录下templates文件夹中读取压缩包
 * @type {{ name: String, url: String?, tplName: String? }[]}
 */
export const templateList = []
