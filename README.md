# Custom Project CLI

1. `git clone` or download this project
2. install the dependencies
3. run `npm link` make a symbol link
4. modify the `config.js` and the command in `package.json`, only supports `tar.gz`
5. run your personal command in terminal~


# 自定义项目创建脚手架

1. `git clone`或者直接下载此项目
2. 安装项目依赖
3. 使用 `npm link` 创建一个符号链接
4. 修改 `config.js`中的配置和package.json中的命令名，仅支持 `tar.gz`文件作为模板文件
5. 执行你修改后的命令~
