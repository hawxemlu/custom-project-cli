import * as commander from "commander";
import chalk from "chalk";
import pkg from "../package.json" assert { type: "json" };
import cli from "../lib/projectCLI.js";
import { log } from "../lib/utils.js";

const program = new commander.Command();
const cmd = Object.keys(pkg.bin)[0];

function runCommand() {
  program
    .version(pkg.version)
    .usage('<command> [options]')

  program.on("--help", () => {
    log(`
      运行 ${chalk.cyan(`${cmd} <command> --help`)} 获得更详细的用法
    `)
  })

  program
    .argument("<command>")
    .action((cmd) => {
      program.outputHelp();
      log(`
        ${chalk.red(`没有找到命令: ${chalk.yellow(cmd)}`)}
      `)
    })

  program
    .command("create <project-name>")
    .description("创建新项目")
    .action((name) => {
      cli.createProject(name);
    })

  program
    .command("clean <project-name>")
    .description("清除文件/文件夹")
    .action((name) => {
      cli.cleanProject(name);
    })

  program.parse(process.argv)

  if (!process.argv.slice(2).length) {
    program.outputHelp();
  }
}

export default runCommand;