#!/usr/bin/env node
import checkUpdate from "../lib/checkUpdate.js";
import runCommand from "./command.js";

checkUpdate(() => {
  runCommand();
})