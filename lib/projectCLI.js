import inquirer from "inquirer";
import chalk from "chalk";
import fs from "fs";
import path from "path";
import url from "url";
import tar from "tar";
import { templateList } from "../config.js";
import { done, warn, error, exit, logWithSpinner, stopSpinner, downloadTemplate } from "./utils.js";
import { info } from "console";

class CLI {
  /**
   * @param {String?} name 
   */
  constructor(name) {
    if (!name) return;
    this.createProject(name);
  }

  /**
   * @returns {{ name: String, url: String, tplName: String }}
   */
  async selectTemplate() {
    if (templateList.length === 0) {
      error("不存在模板配置，请配置模板列表后使用");
      exit();
    }
    const { template } = await inquirer.prompt([{
      name: "template",
      type: "list",
      message: "即将创建一个项目，请选择使用的模板：",
      choices: templateList,
    }])
    return templateList.filter(tpl => tpl.name === template)[0];
  }

  /**
   * @param {String} projectName 
   * @returns {{ basePath: String, projectName: String, description: String }}
   */
  async checkAndPrepare(projectName) {   
    const { checkName } = await inquirer.prompt([{
      name: "checkName",
      type: "confirm",
      message: `确认使用【${projectName}】作为项目名称?`,
    }])
  
    if (!checkName) {
      done(`${chalk.bgGreen.black(" DONE ") }CLI已退出，请重新初始化`);
      exit();
      return;
    }
  
    logWithSpinner('✨', '正在查询项目名称是否可用...');
    const basePath = path.resolve("./");
    const paths = fs.readdirSync(basePath);
    stopSpinner();
    if (paths.includes(projectName)) {
      warn("当前路径下有相同项目文件夹，请重新命名或更改项目创建位置");
      exit();
      return;
    }
    info("项目名称可用")
  
    const { description } = await inquirer.prompt([{
      name: "description",
      type: "input",
      message: "请输入项目描述",
    }])
  
    if (!description) {
      error("项目描述不可为空");
      exit();
      return;
    }

    return { basePath, projectName, description };
  }

  /**
     * @param {String} destPath 
     * @param {{ name: String, url: String, tplName: String }} template 
     */
  async loadTemplate(destPath, template) {
    const __dirname = path.dirname(url.fileURLToPath(import.meta.url));
    const tplName = template.tplName.indexOf("tar.gz") > 0 ? template.tplName : `${template.tplName}.tar.gz`;
    const zipPath = path.join(__dirname, "../templates/", tplName);
    fs.mkdirSync(destPath);
    /* 执行出错后清除下载的包和创建的文件 */
    process.addListener("uncaughtException", (err) => {
      if (fs.existsSync(destPath)) {
        fs.rmSync(destPath, { recursive: true });
      }
      error(` ${err}`);
      exit();
    })
    await tar.extract({
      file: zipPath,
      cwd: destPath,
    })
    const paths = fs.readdirSync(destPath);
    if (paths.length === 1 || !paths.includes("package.json")) {
      const sourcePath = path.resolve(destPath, paths[0])
      fs.cpSync(sourcePath, destPath, { recursive: true });
      fs.rmSync(sourcePath, { recursive: true });
    }
  }

  /**
   * @param {String} destPath 
   * @param {{ name: String, url: String, tplName: String }} template 
   */
  async downloadTemplate(destPath, template) {
    const zipPath = `${destPath}.tar.gz`;
    fs.mkdirSync(destPath);
    /* 执行出错后清除下载的包和创建的文件 */
    process.addListener("uncaughtException", (err) => {
      if (fs.existsSync(destPath)) {
        fs.rmSync(destPath, { recursive: true });
      }
      if (fs.existsSync(zipPath)) {
        fs.rmSync(zipPath);
      }
      error(` ${err}`);
      exit();
    })
    await downloadTemplate(destPath, template.url)
    await tar.extract({
      file: zipPath,
      cwd: destPath,
    })
    /* 解压完成删除压缩包 */
    fs.rmSync(zipPath);
    const paths = fs.readdirSync(destPath);
    if (paths.length === 1 || !paths.includes("package.json")) {
      const sourcePath = path.resolve(destPath, paths[0])
      fs.cpSync(sourcePath, destPath, { recursive: true });
      fs.rmSync(sourcePath, { recursive: true });
    }
  }

  /**
   * @param {String} destPath 
   * @param {String} projectName 
   * @param {String} description 
   */
  initPackageJson(destPath, projectName, description) {
    const packageJSONPath = path.resolve(destPath, "./package.json")
    const { author, repository, ...json } = JSON.parse(fs.readFileSync(packageJSONPath, { encoding: "utf-8" }));
    json.name = projectName;
    json.description = description;
    json.version = "1.0.0";
    fs.writeFileSync(packageJSONPath, JSON.stringify(json, null, "\t"), { encoding: "utf-8" });
  }

  /**
   * @param {String} destPath 
   * @param {String} projectName 
   * @param {String} description 
   */
  initREADME(destPath, projectName, description) {
    const fileContent = `# ${projectName}\n${description}`;
    const READMEPath = path.resolve(destPath, "./README.md");
    fs.writeFileSync(READMEPath, fileContent);
  }

  /**
   * @param {String} destPath 
   * @param {String} projectName 
   */
  initCHANGELOG(destPath, projectName) {
    const fileContent = `# ${projectName}\n## 1.0.0\n初始化项目`;
    const CHANGELOGPath = path.resolve(destPath, "./CHANGELOG.md");
    fs.writeFileSync(CHANGELOGPath, fileContent);
  }

  /**
   * @param {String} name 
   */
  async createProject(name) {
    const template = await this.selectTemplate();
    const { basePath, projectName, description } = await this.checkAndPrepare(name);
    const destPath = path.resolve(basePath, projectName);
    if (!template.tplName) {
      logWithSpinner('⚓', `正在下载项目模板 (${chalk.yellow(template.name)})...`)
      await this.downloadTemplate(destPath, template);
      stopSpinner();
      done("模板下载完成");
    } else {
      logWithSpinner('⚓', `正在加载项目模板 (${chalk.yellow(template.name)})...`)
      await this.loadTemplate(destPath, template);
      stopSpinner();
      done("模板加载完成");
    }
    logWithSpinner("🗃", "正在初始化项目...");
    this.initPackageJson(destPath, projectName, description);
    this.initREADME(destPath, projectName, description);
    this.initCHANGELOG(destPath, projectName);
    stopSpinner();
    done(`${chalk.green('项目初始化完成，(゜-゜)つロ 干杯~')}`)
    done(`打开 ${chalk.yellow(projectName)} 文件夹开始敲代码吧～`)
    done(`记得 ${chalk.yellow('yarn install')} 安装所需的依赖包。`)
    exit();
  }

  /**
   * @param {String} name 
   */
  async cleanProject(name) {
    const cleanPath = path.resolve(name);
    if (!fs.existsSync(cleanPath)) {
      warn("目录不存在，CLI已退出");
      exit();
      return;
    }
    const { checkClean } = await inquirer.prompt([{
      name: "checkClean",
      type: "confirm",
      message: `确认清除对应路径文件/文件夹？`,
      default: false
    }])
    if (!checkClean) {
      done("CLI已退出");
      exit();
      return;
    }
    logWithSpinner("🗃", "正在清除文件/文件夹...");
    fs.rmSync(cleanPath, { recursive: true });
    stopSpinner();
    done(`${chalk.green('文件/文件夹清理完成，(゜-゜)つロ 干杯~')}`)
    exit();
  } 
}

export default new CLI();