import chalk from "chalk";
import ora from "ora";
import axios from "axios";
import fs from "fs";

function log(msg) {
  console.log(msg);
}

function done(msg) {
  console.log(`${chalk.bgGreen.black(" DONE ")} ${msg}`);
}

function warn(msg) {
  console.warn(`${chalk.bgYellow.black(" WARN ")} ${chalk.yellow(msg)}`);
}

function error(msg) {
  console.log(`${chalk.bgRed(" ERROR ")} ${chalk.red(msg)}`);
}

function exit() {
  process.exit();
}

const spinner = ora();
let lastMsg = null;

function logWithSpinner(symbol, msg) {
  if (!msg) {
    msg = symbol
    symbol = chalk.green('✔')
  }
  if (lastMsg) {
    spinner.stopAndPersist({
      symbol: lastMsg.symbol,
      text: lastMsg.text
    })
  }
  spinner.text = ` ${msg}`
  lastMsg = {
    symbol: `${symbol} `,
    text: msg,
  }
  spinner.start()
}

function stopSpinner(persist) {
  if (!spinner.isSpinning) {
    return;
  }

  if (lastMsg && persist !== false) {
    spinner.stopAndPersist({
      symbol: lastMsg.symbol,
      text: lastMsg.text
    })
  } else {
    spinner.stop()
  }
  lastMsg = null;
}

/**
 * @param {String} path 不带压缩后缀的路径
 * @param {String} url 下载路径
 */
function downloadTemplate(path, url) {
  return new Promise((resolve) => {
    axios({
      method: "get",
      url,
      responseType: "stream"
    }).then((res) => {
      res.data.pipe(fs.createWriteStream(`${path}.tar.gz`));
      res.data.on("close", resolve)
    })
  })
}

export { log, done, warn, error, exit, logWithSpinner, stopSpinner, downloadTemplate };