import updater from "pkg-updater";
import pkg from "../package.json" assert { type: "json" };
import { updateConfig } from "../config.js";

/**
 * @param {Function?} cb 
 * @returns 
 */
function checkUpdate(cb) {
  if (!updateConfig.registry) {
    cb && cb();
    return;
  }
  updater({
    pkg,
    ...updateConfig,
  })
  .then(() => {
    cb && cb() // cb回调启动cli
  })
}

export default checkUpdate;
